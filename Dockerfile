#### CONSTRUCCION MVN PACKAGE (JAR) ####
FROM maven:3.6-jdk-8-alpine AS builder

WORKDIR /app

COPY pom.xml .
RUN mvn dependency:resolve

COPY src ./src
RUN mvn clean install -DskipTests



#### EJECUCION DEL JAR ####
FROM openjdk:8-jdk-alpine
LABEL maintainer="fsayalat@gmail.com"

COPY --from=builder /app/target/reto*.jar /app.jar

COPY agencias.json /agencias.json

#ENV host="mysql_server"
#ENV port="3306"
#ENV database="softpya_campus_db"
#ENV username="root"
#ENV password="Peru123.@"

ENTRYPOINT exec java -jar /app.jar
EXPOSE 8080