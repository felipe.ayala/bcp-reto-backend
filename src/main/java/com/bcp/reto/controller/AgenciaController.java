package com.bcp.reto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bcp.reto.dto.AgenciaDTO;
import com.bcp.reto.service.IAgenciaService; 

@RestController
@RequestMapping("/api/agencia")
public class AgenciaController {

	@Autowired
	private IAgenciaService service;
 
	@GetMapping(value = "/{nombre}", produces = "application/json")
	public List<AgenciaDTO> obtenerPorNombre(@PathVariable("nombre") String nombre) {
		return service.obtenerPorNombre(nombre);
	}
	
}
