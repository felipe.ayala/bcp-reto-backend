package com.bcp.reto.service.Imp;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bcp.reto.dao.IAgenciaDAO; 
import com.bcp.reto.dto.AgenciaDTO;
import com.bcp.reto.service.IAgenciaService; 

@Service
public class AgenciaServiceImpl implements IAgenciaService{

	@Autowired
	private IAgenciaDAO dao;

	@Override
	public AgenciaDTO registrar(AgenciaDTO t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AgenciaDTO modificar(AgenciaDTO t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void eliminar(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<AgenciaDTO> obtenerPorNombre(String agencia) {
		// TODO Auto-generated method stub
		List<AgenciaDTO> agencias =  dao.obtenerAgencias();
		List<AgenciaDTO> optionalAgencia = agencias.stream().
				filter(p->p.getAgencia().toLowerCase().contains(agencia))
				.collect(Collectors.toList());
		return optionalAgencia;
	}

	@Override
	public List<AgenciaDTO> listar() {
		// TODO Auto-generated method stub
		return null;
	}
 

}
