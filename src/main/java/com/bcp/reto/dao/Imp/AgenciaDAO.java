package com.bcp.reto.dao.Imp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.stereotype.Service;

import com.bcp.reto.dao.IAgenciaDAO;
import com.bcp.reto.dto.AgenciaDTO;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class AgenciaDAO implements IAgenciaDAO{
	
	@Override
	public List<AgenciaDTO> obtenerAgencias()
	{
		List<AgenciaDTO> lstAgencias = null;
		try
		{
			lstAgencias = new ArrayList<AgenciaDTO>();
			ObjectMapper mapper = new ObjectMapper();
			InputStream inputStream = new FileInputStream(new File("/agencias.json"));
			TypeReference<List<AgenciaDTO>> typeReference = new TypeReference<List<AgenciaDTO>> () {};
			lstAgencias = mapper.readValue(inputStream, typeReference);
		}catch(FileNotFoundException e)
		{
			
		}
		catch(JsonParseException e)
		{
			
		}
		catch(JsonMappingException e)
		{
			
		}
		catch(IOException e)
		{
			
		}
		return lstAgencias;
	}
}
