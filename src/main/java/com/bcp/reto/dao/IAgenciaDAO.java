package com.bcp.reto.dao;

import java.util.List;

import com.bcp.reto.dto.AgenciaDTO;

public interface IAgenciaDAO {
	
	List<AgenciaDTO> obtenerAgencias();
}
